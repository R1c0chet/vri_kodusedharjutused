
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>Kodune ülesanne nr 8</title>

    <?php
    $text="Input text";
    if (isset($_POST["input"]) && $_POST["input"]!="") {
        $text=htmlspecialchars($_POST["input"]);
    }
    $bg_color="#fff";
    if (isset($_POST['taustaVarv']) && $_POST['taustaVarv']!="") {
        $bg_col=htmlspecialchars($_POST['taustaVarv']);
    }
    $txt_column="#000";
    if (isset($_POST['tekstiVarv']) && $_POST['tekstiVarv']!="") {
        $txt_col=htmlspecialchars($_POST['tekstiVarv']);
    }
    $brd_clr="#000";
    if (isset($_POST['piirjooneVarv']) && $_POST['piirjooneVarv']!="") {
        $brd_clr=htmlspecialchars($_POST['piirjooneVarv']);
    }
    $brd_style="solid";
    if (isset($_POST['piirjooneStiil']) && $_POST['piirjooneStiil']!="") {
        $brd_style=htmlspecialchars($_POST['piirjooneStiil']);
    }
    $brd_wdth="1px";
    if (isset($_POST['piirjooneLaius']) && $_POST['piirjooneLaius']!="") {
        $brd_wdth=htmlspecialchars($_POST['piirjooneLaius']);
    }
    $arc_rad="0px";
    if (isset($_POST['nurgaRaadius']) && $_POST['nurgaRaadius']!="") {
        $arc_rad=htmlspecialchars($_POST['nurgaRaadius']);
    }
    ?>

    <style>
        #ekraan {
            height: 75px;
            width: 300px;
            margin-bottom: 50px;
            background-color: <?php echo $bg_color; ?>;
            color: <?php echo $txt_column; ?>;
            border-width: <?php echo $brd_wdth."px"; ?>;
            border-style: <?php echo $brd_style; ?>;
            border-color: <?php echo $brd_clr; ?>;
            border-radius: <?php echo $arc_rad."px"; ?>;
        }
    </style>
</head>

<body>
<div id = ekraan>
    <?php echo $text; ?>
</div>

<div>
    <form method="post" name="form">
        <textarea name="input" rows="3" cols="20" placeholder="Sisesta tekst"></textarea><br/>
        <br/>
        <input type="color" name="taustaVarv" value="#ffffff">Taustavärvus<br/>
        <br/>
        <input type="color" name="tekstiVarv" value="#000000">Tekstivärvus<br/>
        <br/>
        <input type="number" name="piirjooneLaius" min="0" max="20"> Piirjoone laius<br/>
        <br/>
        <select name="piirjooneStiil">
            <option value ="none">none</option>
            <option value ="dotted">dotted</option>
            <option value ="dashed">dashed</option>
            <option value ="solid">solid</option>
            <option value ="double">double</option>
            <option value ="groove">groove</option>
            <option value ="ridge">ridge</option>
            <option value ="inset">inset</option>
            <option value ="outset">outset</option>
        </select>Piirjoone stiil<br/>
        <br/>
        <input type="color" name="piirjooneVarv" value="#ffffff">Piirjoone värvus<br/>
        <br/>
        <input type="number" name="nurgaRaadius" min="0" max="25">Piirjoone nurga raadius(0-25px)<br/>
        <br/>
        <input type="submit" value="esita">
    </form>
</div>
</body>
</html>