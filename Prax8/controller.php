﻿<?php
//ini_set("display_errors", 1);

$images=array(
  array(0, "big"=>"img/assassinscreed_800x450.jpg", "small"=>"thumb/assassinscreed_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(1, "big"=>"img/bioshock_800x450.jpg", "small"=>"thumb/bioshock_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(2, "big"=>"img/dishonored_800x450.jpg", "small"=>"thumb/dishonored_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(3, "big"=>"img/halo5_800x450.jpg", "small"=>"thumb/halo5_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(4, "big"=>"img/stalker_800x450.jpg", "small"=>"thumb/stalker_150x84.jpg", "alt"=>"Author: Jan Eerik")  	
 );

$mode = 'main';
$mainContentView = 'view/main.html';
if (isset($_GET['mode'])) {
    $mode = $_GET['mode'];
    switch($mode) {
        case 'main':
            $mainContentView = 'view/main.html';
            break;
        case 'gallery':
            $mainContentView = 'view/gallery.html';
            break;
        case 'upload':
            $mainContentView = 'view/upload.html';
            break;
        case 'login':
            $mainContentView = 'view/login.html';
            break;
        case 'register':
            $mainContentView = 'view/register.html';
            break;
        case 'image':
            $mainContentView = 'view/image.html';
            $imageId = 0;
            if (isset($_GET['id'])){
                if(!is_numeric($_GET['id']) || !array_key_exists($_GET['id'],$images)) break;
                else $imageId = $_GET['id'];
                $activeImage = $images[$imageId];
            }
            break;
        default:
            $mainContentView = 'view/404.html';
    }
}

include_once 'view/head.html';
include $mainContentView;
include_once 'view/foot.html';

?>
