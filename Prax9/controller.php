﻿<?php
ini_set("display_errors", 1);

require_once('functions.php');

$images=array(
  array(0, "big"=>"img/assassinscreed_800x450.jpg", "small"=>"thumb/assassinscreed_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(1, "big"=>"img/bioshock_800x450.jpg", "small"=>"thumb/bioshock_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(2, "big"=>"img/dishonored_800x450.jpg", "small"=>"thumb/dishonored_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(3, "big"=>"img/halo5_800x450.jpg", "small"=>"thumb/halo5_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array(4, "big"=>"img/stalker_800x450.jpg", "small"=>"thumb/stalker_150x84.jpg", "alt"=>"Author: Jan Eerik")  	
 );
 
 $error_messages = array();

$mode = 'main';
$mainContentView = 'view/main.html';
if (isset($_GET['mode'])) {
    $mode = $_GET['mode'];
    switch($mode) {
        case 'main':
            show_main();
            break;
        case 'gallery':
            show_gallery();
            break;
        case 'upload':
            show_upload();
            break;
        case 'login':
            show_login();
            break;
        case 'register':
            show_register();
            break;
        case 'image':
            show_image();
            break;
        default:
            show_error('404');
    }
} else {
    show_main();
}

?>
