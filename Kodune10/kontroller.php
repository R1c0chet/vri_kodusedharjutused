
<?php
session_start();
require_once('vaated/head.html');
require_once('vaated/pildid.php');
$mode = 'pealeht';
if (!empty($_GET)) {
    if ($_GET["mode"] != "") {
        $mode = $_GET["mode"];
    }
}
switch ($mode) {
    case 'galerii':
        require_once('vaated/galerii.php');
        break;
    case 'vote':
        if (array_key_exists("vote", $_SESSION)){
            require_once ('vaated/tulemus.php');
            break;
        }
        require_once('vaated/vote.php');
        break;
    case 'tulemus':
        require_once('vaated/tulemus.php');
        break;
    case 'logout':
        unset($_SESSION["vote"]);
        header("Location: kontroller.php?mode=vote");
        break;
    default:
        require_once('vaated/pealeht.php');
}
require_once('vaated/foot.html'); ?>