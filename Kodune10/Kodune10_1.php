﻿
<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <title>Kodune ülesanne nr 8</title>

    <?php
    $text="Input text";
    if (isset($_POST["input"]) && $_POST["input"]!="") {
        $text=htmlspecialchars($_POST["input"]);
    }
    $bg_color="#fff";
    if (isset($_POST['taustaVarv']) && $_POST['taustaVarv']!="") {
        $bg_color=htmlspecialchars($_POST['taustaVarv']);
    }
    $txt_col="#000";
    if (isset($_POST['tekstiVarv']) && $_POST['tekstiVarv']!="") {
        $txt_col=htmlspecialchars($_POST['tekstiVarv']);
    }
    $brd_clr="#000";
    if (isset($_POST['piirjooneVarv']) && $_POST['piirjooneVarv']!="") {
        $brd_clr=htmlspecialchars($_POST['piirjooneVarv']);
    }
    $brd_style="solid";
    if (isset($_POST['piirjooneStiil']) && $_POST['piirjooneStiil']!="") {
        $brd_style=htmlspecialchars($_POST['piirjooneStiil']);
    }
    $brd_wdth="1px";
    if (isset($_POST['piirjooneLaius']) && $_POST['piirjooneLaius']!="") {
        $brd_wdth=htmlspecialchars($_POST['piirjooneLaius']);
    }
    $arc_rad="0px";
    if (isset($_POST['nurgaRaadius']) && $_POST['nurgaRaadius']!="") {
        $arc_rad=htmlspecialchars($_POST['nurgaRaadius']);
    }
    ?>

    <style>
        #ekraan {
            height: 75px;
            width: 300px;
            margin-bottom: 50px;
            background-color: <?php echo $bg_color; ?>;
            color: <?php echo $txt_col; ?>;
            border-width: <?php echo $brd_wdth."px"; ?>;
            border-style: <?php echo $brd_style; ?>;
            border-color: <?php echo $brd_clr; ?>;
            border-radius: <?php echo $arc_rad."px"; ?>;
        }
    </style>
</head>

<body>
<div id = ekraan>
    <?php echo $text; ?>
</div>

<div>
    <form action="Kodune10_1.php" method="post" name="form">
        <textarea name="input" rows="3" cols="20" placeholder='<?php echo "$text";?>'><?php echo "$text";?></textarea><br/>
        <br/>
        <input type="color" name="taustaVarv" value='<?php echo "$bg_color";?>'> Taustavärvus<br/>
        <br/>
        <input type="color" name="tekstiVarv" value='<?php echo "$txt_col";?>'> Tekstivärvus<br/>
        <br/>
        <input type="number" name="piirjooneLaius" min="0" max="20" value='<?php echo "$brd_wdth"?>'> Piirjoone laius<br/>
        <br/>
        <select name="piirjooneStiil" value='<?php echo "$brd_style"?>'>
            <option value ="none" <?php if ($brd_style == 'none') echo 'selected';?> >none</option>
            <option value ="dotted" <?php if ($brd_style == 'dotted') echo 'selected';?> >dotted</option>
            <option value ="dashed" <?php if ($brd_style == 'dashed') echo 'selected';?> >dashed</option>
            <option value ="solid" <?php if ($brd_style == 'solid') echo 'selected';?> >solid</option>
            <option value ="double" <?php if ($brd_style == 'double') echo 'selected';?> >double</option>
            <option value ="groove" <?php if ($brd_style == 'groove') echo 'selected';?> >groove</option>
            <option value ="ridge" <?php if ($brd_style == 'ridge') echo 'selected';?> >ridge</option>
            <option value ="inset" <?php if ($brd_style == 'inset') echo 'selected';?> >inset</option>
            <option value ="outset" <?php if ($brd_style == 'outset') echo 'selected';?> >outset</option>
        </select>Piirjoone stiil<br/>
        <br/>
        <input type="color" name="piirjooneVarv" value='<?php echo "$brd_clr";?>'> Piirjoone värvus<br/>
        <br/>
        <input type="number" name="nurgaRaadius" value='<?php echo "$arc_rad"; ?>' min="0" max="25"> Piirjoone nurga raadius(0-25px)<br/>
        <br/>
        <input type="submit" value="esita">
    </form>
</div>
</body>
</html>