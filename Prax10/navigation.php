
<div class="menu">
    <ul>
        <li>
            <a href="?mode=main">Avaleht</a>
        </li>
        <li>
            <a href="?mode=gallery">Galerii</a>
        </li>

        <?php if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] == false): ?>
        <li>
            <a href="?mode=login">Logi sisse</a>
        </li>
        <li>
            <a href="?mode=register">Loo uus konto</a>
        </li>
        <?php endif; ?>

        <?php if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true): ?>
        <li>
            <a href="?mode=upload">Laadi üles</a>
        </li>
        <li>
        <a href="?mode=logout">Logi välja</a>
        </li>
        <?php endif; ?>

    </ul>
</div>