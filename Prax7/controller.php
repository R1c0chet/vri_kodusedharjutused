﻿<?php
ini_set("display_errors", 1);
// Siia kirjutan PHP koodi
$pictures=array(
  array("big"=>"img/assassinscreed_800x450.jpg", "small"=>"thumb/assassinscreed_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array("big"=>"img/bioshock_800x450.jpg", "small"=>"thumb/bioshock_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array("big"=>"img/dishonored_800x450.jpg", "small"=>"thumb/dishonored_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array("big"=>"img/halo5_800x450.jpg", "small"=>"thumb/halo5_150x84.jpg", "alt"=>"Author: Jan Eerik"),
  array("big"=>"img/stalker_800x450.jpg", "small"=>"thumb/stalker_150x84.jpg", "alt"=>"Author: Jan Eerik")  	
 );

include_once('views/head.html');
include('views/gallery.html');
include_once('views/foot.html');

?>
